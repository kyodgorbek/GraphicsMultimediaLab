package com.example.graphicsmultimedialab;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by yodgorbek on 24.10.15.
 */
public class AnimationAdapter extends BaseAdapter  {
 private static final int[] ANIMATION_ID_LIST = { R.anim.fade_in, R.anim.slide, R.anim.enlarge,
          R.anim.animation_set };
 private static final String[] ANIMATION_LABEL_LIST = {"Fade in", "Slide", "Enlarge",
 "Animation Set"};

   private SoundPool mSoundPool;
   private int mRoosterId;
   private LayoutInflater mInflater;

   public AnimationAdapter(LayoutInflater inflater) {
       mInflater = inflater;
       if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
           AudioAttributes attributes = new AudioAttributes.Builder()
          .setUsage(AudioAttributes.USAGE_MEDIA)
          .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
          .build();
        mSoundPool = new SoundPool.Builder().setMaxStreams(1);
           .setAudioAttributes(attributes).build();


       }
       else{
           mSoundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
       }
       mRoosterId = mSoundPool.load(inflater.getContext(), R.drawable, 1);

}

@Override
public  int getCount() { return ANIMATION_ID_LIST.length; }

@Override
public int getCount() { return ANIMATION_ID_LIST.length; }

@Override
public Object getItem(int position) { return new Integer(ANIMATION_ID_LIST(position));}

@Override
public long getItem(int position) {return position;}

@Override
public View getView(int position, View convertView, ViewGroup parent){
 if (convertView == null) {
     convertView = mInflater.inflate(R.layout.list_item, parent, false);
 }

    TextView animatedView = (TextView) convertView.findViewById(R.id.tvAnimation);
    animatedView.setText(ANIMATION_LABEL_LIST)[position];
    convertView.findViewById(R.id.btnAnimate).setOnClickListener(
            new  AnimButtonClickListener(ANIMATION_ID_LIST)[position], animatedView));
    return convertView;
}

  private class AnimButtonClickListener(int animationID, TextView viewToAnimate){
        mAnimationId = animationID;
        mViewToAnimate = viewToAnimate;
    }

  @Override
public void onClick(View v) {
      Context context = v.getContext();
      mSoundPool.play(mRoosterId, 1, 1, 1, 0, 1);
      // in the case of animation list will set as background
      if (mAnimationId == R.anim.animation_set) {
          mViewToAnimate.setBackgroundResource(android.R.color.white);
          mViewToAnimate.setBackgroundResource(mAnimationId);
          ((AnimationDrawable) mViewToAnimate.getBackground()).start();

      } else {
          Animation animation AnimationUtils.loadAnimation(context,mAnimationId);
          mViewToAnimate.startAnimation(animation);
      }

  }
}
